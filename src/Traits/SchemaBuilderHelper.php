<?php

namespace Tarre\Helpers\Traits;

use Closure;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

trait SchemaBuilderHelper
{
    /**
     * Defines the PIVOT table for a manyToMany relationship with optional closure to extend with &$table. The pivot table name is returned
     *
     * @param $table1
     * @param $table2
     * @param null|closure $callback
     * @return string
     */
    private function manyToMany($table1, $table2, $callback = null, $bigInteger = false)
    {
        // create pivot table
        $pivot_table = $this->createPivotTable($table1, $table2);

        // create table
        Schema::create($pivot_table, function (Blueprint $table) use ($callback, $table1, $table2, $bigInteger) {
            if ($bigInteger) {
                $table->bigIncrements('id');
            } else {
                $table->increments('id');
            }
            // add constraints
            $this->hasConstraintTo($table, $table1, null, false, $bigInteger);
            $this->hasConstraintTo($table, $table2, null, false, $bigInteger);
            // check for callback
            if ($callback instanceof Closure) {
                $callback($table);
            }
        });

        // return pivot table name
        return $pivot_table;
    }

    /**
     * Defines the PIVOT table for a manyToMany relationship with optional closure to extend with &$table. The pivot table name is returned
     *
     * @param $table1
     * @param $table2
     * @param null|closure $callback
     * @return string
     */
    private function manyToManyBig($table1, $table2, $callback = null)
    {
        return $this->manyToMany($table1, $table2, $callback, true);
    }

    /**
     * Drops a pivot table
     *
     * @param $table1
     * @param $table2
     */
    private function dropManyToMany($table1, $table2)
    {
        Schema::dropIfExists($this->createPivotTable($table1, $table2));
    }


    /**
     * Creates a preffered foregin key constraint to another table
     *
     * @param Blueprint $table
     * @param $to
     * @param bool $localKey
     * @param bool $nullable
     * @return mixed
     */
    private function hasConstraintTo(Blueprint &$table, $to, $localKey = null, $nullable = false, $bigInteger = false)
    {
        $localKey = !$localKey ? Str::singular($to) . '_id' : $localKey;

        if ($bigInteger) {
            if ($nullable) {
                $table->bigInteger($localKey)->unsigned()->nullable();
            } else {
                $table->bigInteger($localKey)->unsigned();
            }
        } else {
            if ($nullable) {
                $table->integer($localKey)->unsigned()->nullable();
            } else {
                $table->integer($localKey)->unsigned();
            }
        }


        $prim_key = $table->getTable() . '_' . $to . $localKey;
        //$prim_key = strlen($prim_key) > 64 ? substr($prim_key, 0, -(strlen($prim_key) - 64)) : $prim_key;
        // Shorten name for index and foregein keyz
        if (strlen($prim_key) > 64) {
            $prim_key = preg_replace('/([a-z]{0,2})[^_]*/', '$1', $prim_key);
        }

        $table->index($localKey, $prim_key);

        return $table->foreign($localKey, $prim_key)->references('id')->on($to); // Method chain continues
    }

    /**
     * Creates a preffered foregin key constraint to another table
     *
     * @param Blueprint $table
     * @param $to
     * @param bool $localKey
     * @param bool $nullable
     * @return mixed
     */
    private function hasBigConstraintsTo(Blueprint &$table, $to, $localKey = null, $nullable = false)
    {
        return $this->hasConstraintTo($table, $to, $localKey, $nullable, true);
    }

    // Make constraints outside of Schema

    /**
     * Like hasConstraintTo but takes an $table_name instead of an Blueprint &$table
     *
     * @param $tableName
     * @param $to
     * @param bool $localKey
     */
    private function hasConstraintToRAW($tableName, $to, $localKey = null)
    {
        $localKey = !$localKey ? Str::singular($to) . '_id' : $localKey;

        DB::statement('ALTER TABLE `' . $tableName . '` ADD `' . $localKey . '` INT(10) UNSIGNED NULL AFTER `id`;');
        DB::statement('ALTER TABLE `' . $tableName . '` ADD INDEX(`' . $localKey . '`);');
        DB::statement('ALTER TABLE `' . $tableName . '` ADD CONSTRAINT `bfk_' . $localKey . '` FOREIGN KEY (`' . $localKey . '`) REFERENCES `' . $to . '`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;');
    }

    /**
     * Like hasConstraintTo but takes an $table_name instead of an Blueprint &$table
     *
     * @param $tableName
     * @param $to
     * @param bool $localKey
     */
    private function hasBigConstraintToRAW($tableName, $to, $localKey = null)
    {
        $localKey = !$localKey ? Str::singular($to) . '_id' : $localKey;

        DB::statement('ALTER TABLE `' . $tableName . '` ADD `' . $localKey . '` BIGINT(20) UNSIGNED NULL AFTER `id`;');
        DB::statement('ALTER TABLE `' . $tableName . '` ADD INDEX(`' . $localKey . '`);');
        DB::statement('ALTER TABLE `' . $tableName . '` ADD CONSTRAINT `bfk_' . $localKey . '` FOREIGN KEY (`' . $localKey . '`) REFERENCES `' . $to . '`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;');
    }

    public function createPivotTable($table1, $table2)
    {
        // We do this so we can sort table by name
        $tables = [Str::singular($table1), Str::singular($table2)];
        sort($tables);
        return $tables[0] . '_' . $tables[1];
    }
}
