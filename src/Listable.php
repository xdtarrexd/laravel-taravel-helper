<?php


namespace Tarre\Helpers;


use Carbon\Carbon;
use http\Exception\InvalidArgumentException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Listable
{
    /**
     * Get relation with only id
     * @param Model $model
     * @param $manyRelation
     */
    public static function getter(Model $model, $manyRelation)
    {
        if (!$model->exists) {
            return [];
        }

        return $model->$manyRelation()->get()->pluck('id');
    }

    /**
     * Set relation with only id
     * @param Model $model
     * @param $manyRelation
     * @param array $rows
     */
    public static function setter(Model $model, $manyRelation, array $rows)
    {
        $config = self::getConfig($model, $manyRelation);
        /** @var $targetModel BelongsToMany|HasMany|HasManyThrough */
        $targetModel = $config['targetModel'];
        /*
         * If the record exists, we continue business as usual
         */
        if ($model->exists) {
            $targetModel->sync($rows);
            return;
        }
        /*
         * Otherwise, await model creation, then sync
         */
        $model::created(function () use ($targetModel, $rows) {
            $targetModel->sync($rows);
        });
    }

    /**
     * Get relation with pivot data
     * @param Model $model
     * @param $manyRelation
     */
    public static function getterEx(Model $model, $manyRelation)
    {
        if (!$model->exists) {
            return [];
        }

        $config = self::getConfig($model, $manyRelation);

        $attributes = $config['attributes'];
        /*
         * Assume we want id
         */
        if (!isset($attributes['id'])) {
            $attributes[] = 'id';
        }
        $pivots = $config['pivots'];
        $casts = $config['casts'];

        /** @var $targetModel BelongsToMany|HasMany|HasManyThrough */
        $targetModel = $config['targetModel'];

        return $targetModel->withPivot($pivots)
            ->get()
            ->map(function ($model) use ($attributes, $pivots, $casts) {
                $aRet = [];
                /*
                 * Fill attributes
                 */
                foreach ($attributes as $attribute) {
                    $aRet[$attribute] = $model[$attribute];
                }
                /*
                 * Fill pivots
                 */
                foreach ($pivots as $pivotName) {
                    $aRet["pivot_$pivotName"] = $model->pivot->{$pivotName};
                }
                /*
                 * Cast pivots
                 */
                foreach ($casts as $finishedKey => $type) {
                    $aRet["pivot_$finishedKey"] = self::castAsGetter($type, $aRet["pivot_$finishedKey"]);
                }
                /*
                 * Return data
                 */
                return $aRet;
            });
    }

    /**
     * Set relation with pivot data
     * @param Model $model
     * @param $manyRelation
     * @param array $rows
     */
    public static function setterSync(Model $model, $manyRelation, array $rows)
    {
        /*
         * If the record exists, we continue business as usual
         */
        if ($model->exists) {
            self::syncOrAttach($model, $manyRelation, $rows);
            return;
        }
        /*
         * Otherwise, await model creation, then sync
         */
        $model::created(function () use ($model, $manyRelation, $rows) {
            self::syncOrAttach($model, $manyRelation, $rows);
        });
    }

    public static function setterAttach(Model $model, $manyRelation, array $rows)
    {
        /*
    * If the record exists, we continue business as usual
    */
        if ($model->exists) {
            self::syncOrAttach($model, $manyRelation, $rows, true);
            return;
        }
        /*
         * Otherwise, await model creation, then sync
         */
        $model::created(function () use ($model, $manyRelation, $rows) {
            self::syncOrAttach($model, $manyRelation, $rows, true);
        });
    }

    protected static function syncOrAttach(Model $model, $manyRelation, array $rows, $attachInstead = false)
    {
        $config = self::getConfig($model, $manyRelation);

        $pivots = $config['pivots'];
        $casts = $config['casts'];

        /** @var $targetModel BelongsToMany|HasMany|HasManyThrough */
        $targetModel = $config['targetModel'];

        if ($attachInstead) {
            // Remove all
            $targetModel->sync([]);
            // Attach each
            collect($rows)
                ->each(function ($row) use ($targetModel, $pivots, $casts) {
                    $newPivots = [];
                    /*
                     * Cast pivots
                     */
                    foreach ($casts as $finishedKey => $type) {
                        $row["pivot_$finishedKey"] = self::castAsSetter($type, $row["pivot_$finishedKey"]);
                    }
                    /*
                     * Translate pivots
                     */
                    foreach ($pivots as $pivotName) {
                        $newPivots[$pivotName] = $row["pivot_$pivotName"];
                    }
                    /*
                     * Attach single
                     */
                    $targetModel->attach($row['id'], $newPivots);
                });

        } else {

            $ids = collect($rows)
                ->mapWithKeys(function (array $row) use ($pivots, $casts) {
                    $newPivots = [];
                    /*
                     * Cast pivots
                     */
                    foreach ($casts as $finishedKey => $type) {
                        $row["pivot_$finishedKey"] = self::castAsSetter($type, $row["pivot_$finishedKey"]);
                    }
                    /*
                     * Translate pivots
                     */
                    foreach ($pivots as $pivotName) {
                        $newPivots[$pivotName] = $row["pivot_$pivotName"];
                    }
                    /*
                     * Attach to id
                     */
                    return [
                        $row['id'] => $newPivots
                    ];
                });
            /*
            * Sync
            */
            $targetModel->sync($ids);
        }
    }

    /**
     * @param Model $model
     * @param $manyRelation
     * @return array
     */
    protected static function getConfig(Model $model, $manyRelation)
    {

        $targetAttributes = $model->listable[$manyRelation];

        $targetColumns = $targetAttributes['attributes'];
        $targetPivots = $targetAttributes['pivots'] ?? [];
        $casts = $targetAttributes['casts'] ?? [];

        // prepend id
        if (!in_array('id', $targetAttributes)) {
            $targetAttributes[] = 'id';
        }

        return [
            'targetModel' => self::getTargetModel($model, $manyRelation),
            'attributes' => $targetColumns,
            'pivots' => $targetPivots,
            'casts' => $casts
        ];
    }

    /**
     * @param Model $model
     * @param $manyRelation
     * @return BelongsToMany|HasMany|HasManyThrough
     */
    protected static function getTargetModel(Model $model, $manyRelation)
    {
        /** @var $targetModel BelongsToMany|HasMany|HasManyThrough */
        return $model->$manyRelation();
    }

    protected static function castAsGetter($type, $value)
    {
        switch ($type) {
            case 'array':
                return json_decode($value, true);
            case 'boolean':
            case 'bool':
                return (bool)$value;
            case 'string':
                return (string)$value;
            case 'int':
                return (int)$value;
        }
        throw new \InvalidArgumentException("Invalid type \"$type\"");
    }

    protected static function castAsSetter($type, $value)
    {
        switch ($type) {
            case 'array':
                return json_encode($value);
        }
        return $value;
    }

}
